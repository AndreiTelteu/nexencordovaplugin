package be.wearenexen.cordova.options;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class ForegroundScanningOptions {
  private static final String TAG = ForegroundScanningOptions.class.getSimpleName();

  private String mContentTitle = "Background mode";
  private String mStopTitle = "";
  private boolean mEnabled = false;

  public ForegroundScanningOptions() {
    this(null);
  }

  public ForegroundScanningOptions(JSONObject data) {
    if (data != null) {
      parseData(data);
    }
  }

  private void parseData(JSONObject data) {

    try {

      if (data.has("enabled")) {
        mEnabled = data.getBoolean("enabled");
      }

      if (data.has("contentTitle")) {
        mContentTitle = data.getString("contentTitle");
      }

      if (data.has("stopTitle")) {
        mStopTitle = data.getString("stopTitle");
      }

    } catch (JSONException e) {
      Log.e(TAG, "Got en error whilst parsing the options", e);
    }
  }

  public boolean isEnabled() {
    return mEnabled;
  }

  public String getContentTitle() {
    return mContentTitle;
  }

  public String getStopTitle() {
    return mStopTitle;
  }

  @Override
  public String toString() {
    return "ForegroundScanningOptions{" +
      "mEnabled=" + mEnabled +
      ", mContentTitle='" + mContentTitle + '\'' +
      ", mStopTitle='" + mStopTitle + '\'' +
      '}';
  }

}
