//
//  UIDevice+UUID.h
//  NXBeaconSDK
//
//  Created by Mihail Kosyuhin on 7/21/16.
//  Copyright © 2016 Nexen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AdSupport/ASIdentifierManager.h>

@interface UIDevice(UUID)


+ (NSString *)currentDeviceUUID;
+ (BOOL) isAdvertisingTrackingEnabled;
+ (NSString *) advertisingType;

@end
